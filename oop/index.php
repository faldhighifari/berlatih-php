<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';



$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>" ; // 4
echo "Cold Blooded : $sheep->cold_blooded <br><br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


$frog = new Frog("buduk");

echo "Name : $frog->name <br>"; 
echo "Legs : $frog->legs <br>" ; 
echo "Cold Blooded : $frog->cold_blooded <br>";
echo "Jump : ";
$frog->jump();
echo "<br><br>";

$ape = new Ape("kera sakti");

echo "Name : $ape->name <br>"; 
echo "Legs : $ape->legs <br>" ; 
echo "Cold Blooded : $ape->cold_blooded <br>"; 
echo "Yell : ";
$ape->yell();
echo "<br><br>";



?>